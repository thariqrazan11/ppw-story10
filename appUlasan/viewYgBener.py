from django.views.generic import ListView
from django.shortcuts import render
from .models import Ulasan
from django.views.generic import View
from django.http import JsonResponse
from django.forms.models import model_to_dict

# class UlasanListView(View):
#     def get(self, request, format=None):
#         model = Ulasan
#         template_name = 'appUlasan/appUlasan.html' #nama app/htmlnya
#         context_object_name = 'users'

class UlasanListView(View):
    def get(self, request, nama_barang = ''):
        barang = nama_barang
		all_ulasan = Ulasan.objects.all()
		context = {
            'ulasan': all_ulasan,
            'user': request.user,
        }
		return render(request, "appUlasan.html", context) # ,context=context)

class CreateCrudUser(View):
    def get(self, request, nama_barang = ''):
        # nama_barang = request.GET.get('nama_barang', None)
        pesan = request.GET.get('ulasan', None)
        # pengulas_test = request.GET.get('username')
        nama_barang = nama_barang
        usernya = request.user

        obj = Ulasan.objects.create(
            pengulas = usernya,
            nama_barang = nama_barang,
            ulasan = pesan,
        )


        user_loggedin = request.user.username
        username = obj.pengulas.username
        ulasan = {  'id':obj.id, 
                    'pengulas':username, 
                    'ulasan':obj.ulasan, 
                    # 'tanggal_dibuat' : obj.tanggal_dibuat,
                    'user_loggedin':user_loggedin
                    
        }
        # ulasan = model_to_dict(obj)

        data = {
            'ulasan': ulasan
        }
        return JsonResponse(data)

class DeleteCrudUser(View):
    def get(self, request):
        id = request.GET.get('id', None)
        Ulasan.objects.get(id=id).delete()
        data = {
            'deleted': True
        }
        return JsonResponse(data)