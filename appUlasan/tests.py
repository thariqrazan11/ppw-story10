from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from faker import Faker

from .models import Ulasan

import os
import time
import random
import string

# Create your tests here.

class UnitTest(TestCase):
    def test_model(self):
        user = User.objects.create_user(username='testuser', email='testuser@email.com', password='12345')
        ulasan = Ulasan.objects.create(pengulas=user, nama_barang='Tes Barang Baru', ulasan='Tes Barang Baru ini bagus')

        count_ulasan = Ulasan.objects.all().count()
        self.assertEqual(count_ulasan, 1) # Memastikan terbuat

        ulasan.delete()
        user.delete()

        count_ulasan = Ulasan.objects.all().count()
        self.assertEqual(count_ulasan, 0) # Memastikan terhapus

        self.assertEqual(str(ulasan), ulasan.pengulas.username)



class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(FunctionalTest, self).tearDown()

    def test_tulis_ulasan(self):
        user = User.objects.create_user(username='testuser', email='testuser@email.com', password='12345')
        self.client = Client()
        self.client.force_login(user)

        self.browser.get(self.live_server_url + '/ulasan/Tes Tulis Barang')
        time.sleep(5) # Menunggu halaman terbuka

        text_area = self.browser.find_element_by_name('ulasan')
        text_area.clear()
        text_area.send_keys("Tes Tulis Barang Berhasil")

        submit_button = self.browser.find_element_by_xpath("//button[@name='submit-btn']")
        submit_button.click()
        time.sleep(15)

        self.assertEqual("Tes Tulis Barang Berhasil", self.browser.find_element_by_id("col-review").text)

        # javaScript = "document.getElementById('addUlasan').submit();"
        # self.browser.execute_script(javaScript)
        # time.sleep(15)

        # self.assertIn("Tes Tulis Barang Berhasil", self.browser.page_source)

        # submit_button = self.browser.find_element_by_xpath("//button[@name='submit-btn']")
        # self.browser.execute_script("arguments[0].click();", submit_button)
        # time.sleep(15)



        # assert "Tes Tulis Barang Berhasil"

        # ulasannya = WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((By.NAME("ulasanData"))))
        
        # self.assertIn("Tes Tulis Barang Berhasil", self.browser.page_source) # Mengecek apakah nama barang ter-display


    

        

    