# ulasan urls.py
from django.urls import path
from appUlasan import views 

urlpatterns = [
    path('ulasan/<str:nama_barang>/',  views.UlasanListView.as_view(), name='crud_ajax'),
    path('ajax/crud/create/',  views.CreateCrudUser.as_view(), name='crud_ajax_create'),
    path('ajax/crud/delete/',  views.DeleteCrudUser.as_view(), name='crud_ajax_delete'),
]

# urlpatterns = [
#     path('ulasan/<str:nama_barang>/',  views.ulasan_home, name='crud_ajax'),
#     path('ulasan/create/',  views.ulasan_create, name='crud_ajax_create'),
#     path('ulasan/delete/',  views.ulasan_delete, name='crud_ajax_delete'),
# ]


