from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from faker import Faker

import os
import time
import random
import string

# Create your tests here.

class UnitTest(TestCase):
    def test_redirect_to_login_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(FunctionalTest, self).tearDown()
    
    def test_login_logout(self):
        # Membuat akun
        User.objects.create_user('thariq', 'thariq@email.com', 'thariqpassword')

        self.browser.get(self.live_server_url + '/')
 
        time.sleep(5) # Menunggu halaman terbuka
        # driver = webdriver.Chrome()
        # driver.implicitly_wait(30)
        # WebDriverWait(webdriver.Chrome(), 30).until(EC.presence_of_element_located((By.XPATH, '//input[@name="username"]')))

        # LOGIN
        # username = self.browser.find_element_by_id('username')
        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("thariq")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("thariqpassword")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'LOG IN')][1]")
        login_button.click()

        # Ter-redirect ke homepage setelah melalui proses Authentication
        time.sleep(10)
        self.assertIn('thariq', self.browser.page_source) # Memastikan login berhasil

        # LOGUT
        logout_button = self.browser.find_element_by_xpath('//button[contains(.,"LOG OUT")]')
        logout_button.click()
        time.sleep(10)
        self.assertNotIn('thariq', self.browser.page_source) # Memastikan logout berhasil

    def test_login_no_account(self):
        self.browser.get(self.live_server_url + '/')

        time.sleep(5) # Menunggu halaman terbuka

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("thariq")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("thariqpassword")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'LOG IN')][1]")
        login_button.click()

        time.sleep(10)

        # Memastikan error message ditampilkan
        self.assertIn('Username is not registered, please check and try again', self.browser.page_source)

    def test_login_wrong_password(self):
        # Membuat akun
        User.objects.create_user('thariq', 'thariq@email.com', 'thariqpassword')

        self.browser.get(self.live_server_url + '/')

        time.sleep(5) # Menunggu halaman terbuka

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("thariq")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("wrongpassword")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'LOG IN')][1]")
        login_button.click()

        time.sleep(10)

        self.assertIn('Username/password is not match, please check and try again', self.browser.page_source)

    def test_signup(self):
        self.browser.get(self.live_server_url + '/')

        time.sleep(5) # Menunggu halaman terbuka

        sign_up_button = self.browser.find_element_by_xpath('//a[contains(.,"Create one now!")]')
        sign_up_button.click()

        time.sleep(5)

        # SIGN UP
        username = self.browser.find_element_by_xpath('//input[@name="sign_username"]')
        username.send_keys("thariq")

        email = self.browser.find_element_by_xpath('//input[@name="sign_email"]')
        email.send_keys("thariq@email.com")

        password = self.browser.find_element_by_xpath('//input[@name="sign_password"]')
        password.send_keys("thariqpassword")

        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"SIGN UP")]')
        sign_up_button.click()

        time.sleep(5)

        user = authenticate(username="thariq", password="thariqpassword")
        self.assertTrue(user) # Memastikan User is not None

        sign_up_button = self.browser.find_element_by_xpath('//a[contains(.,"Create one now!")]')
        sign_up_button.click()

        time.sleep(5)

        # TEST DUPLICATE USERNAME (setelah ter-redirect)
        username = self.browser.find_element_by_xpath('//input[@name="sign_username"]')
        username.clear()
        username.send_keys("thariq")

        email = self.browser.find_element_by_xpath('//input[@name="sign_email"]')
        email.clear()
        email.send_keys("thariq@gmail.com")

        password = self.browser.find_element_by_xpath('//input[@name="sign_password"]')
        password.clear()
        password.send_keys("thariqpassword")

        # SIGN UP
        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"SIGN UP")]')
        sign_up_button.click()

        time.sleep(5)

        self.assertIn('Username is used, please use another username', self.browser.page_source)
